# README #

A very simple and naive K-NN implementation, and therefore easy to alter at will. Uses a linear search in the case base. Needs some work to be freed from the required libraries:

* Stanford CoreNLP for the parsing of text files (should probably be isolated to a different project)
* Google Guava for the caching of past similarity computations
