/*
 * Copyright 2015 Jérémie Clos <j.clos@rgu.ac.uk>.
 *
 * Licensed under the CRAPL License, Version 0.1 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://matt.might.net/articles/crapl/CRAPL-LICENSE.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rgu.ac.uk.knn.data;

import rgu.ac.uk.knn.nlp.BagOfWords;
import edu.stanford.nlp.util.Sets;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class Aggreg {
    public static enum Aggregate {COLLATE, UNION, INTERSECTION, NONE, SPECIAL, SPECIAL2};
    
    public static String aggregate(String parent, String child, Aggregate aggregation) {
        BagOfWords bow = BagOfWords.createWith(false);
        return aggregate(parent, child, aggregation, bow);
    }
     
    public static String aggregate(String parent, String child, Aggregate aggregation, BagOfWords bow) {
        String result ;
        switch (aggregation) {
            case COLLATE : {
                result = child + " " + parent ;
                break ;
            }
            case UNION : {
                Set<String> bow1 = bow.getBOW(child);
                Set<String> bow2 = bow.getBOW(parent);
                result = Sets.union(bow1, bow2).stream().collect(Collectors.joining(" "));
                break ;
            }
            case INTERSECTION : {
                Set<String> bow1 = bow.getBOW(child);
                Set<String> bow2 = bow.getBOW(parent);
                result = Sets.intersection(bow1, bow2).stream().collect(Collectors.joining(" "));
                break ;
            }
            case SPECIAL : {
                Map<String, Integer> bow1 = bow.getBOWWithFrequencies(child);
                Map<String, Integer> bow2 = bow.getBOWWithFrequencies(parent);
                Map<String, Integer> bowU = BagOfWords.merge(bow1, bow2);
                int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE ;
                for (Map.Entry<String, Integer> entry : bowU.entrySet()) {
                    if (entry.getValue() > max) max = entry.getValue() ;
                    if (entry.getValue() < min) min = entry.getValue() ;
                }
                double middle = (max + min) / 2;
                StringBuilder sb = new StringBuilder();
                for (Map.Entry<String, Integer> entry : bowU.entrySet()) {
                    if (entry.getValue() >= middle) sb.append(entry.getKey()).append(" ");
                }
                result = sb.toString().trim();
                break ;
            }
            case SPECIAL2 : {
                Map<String, Integer> bow1 = bow.getBOWWithFrequencies(child);
                Map<String, Integer> bow2 = bow.getBOWWithFrequencies(parent);
                Map<String, Integer> bowU = BagOfWords.intersect(bow1, bow2);
                int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE ;
                for (Map.Entry<String, Integer> entry : bowU.entrySet()) {
                    if (entry.getValue() > max) max = entry.getValue() ;
                    if (entry.getValue() < min) min = entry.getValue() ;
                }
                double middle = (max + min) / 2;
                StringBuilder sb = new StringBuilder();
                for (Map.Entry<String, Integer> entry : bowU.entrySet()) {
                    if (entry.getValue() >= middle) sb.append(entry.getKey()).append(" ");
                }
                result = sb.toString().trim();
                break ;
            }
            case NONE :
            default : {
                result = child ;
            }
        }
        return result ;
    }
}
