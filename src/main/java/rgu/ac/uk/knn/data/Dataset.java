/*
 * Copyright 2015 Jérémie Clos <j.clos@rgu.ac.uk>.
 *
 * Licensed under the CRAPL License, Version 0.1 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://matt.might.net/articles/crapl/CRAPL-LICENSE.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rgu.ac.uk.knn.data;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import rgu.ac.uk.knn.nlp.BagOfWords;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class Dataset implements Collection<Instance>{
    public static Dataset readChildrenAndParentsIAC(File dataFile, String separator, Predicate<String> labelFilter, Aggreg.Aggregate aggregation, BagOfWords bow) throws IOException {
        Dataset result = new Dataset();
        List<String> data = Files.readAllLines(dataFile.toPath(), Charset.defaultCharset());
        for (String line : data) {
            String[] elements = line.split(separator);
            String child = elements[0];
            String parent = elements[1];
            String instance = Aggreg.aggregate(child, parent, aggregation, bow) ;
            String label = elements[2];
            Instance datum = new Instance(bow.tokenize(instance), label);
            if (labelFilter.test(label)) result.add(datum);
        }
        return result ;
    }
    
    public static Dataset readChildrenAndParentsReddit(File dataFile, String separator, Predicate<String> labelFilter, Aggreg.Aggregate aggregation, BagOfWords bow) throws IOException {
        Dataset result = new Dataset();
        List<String> dataset = Files.readAllLines(dataFile.toPath(), Charset.defaultCharset());
        for (String line : dataset) {
            String[] elements = line.split(separator);
            String child = elements[8];
            String parent = elements[5];
            String instance = Aggreg.aggregate(child, parent, aggregation, bow) ;
            String label = elements[9];
            Instance datum = new Instance(bow.tokenize(instance), label);
            if (labelFilter.test(label)) result.add(datum);
        }
        return result ;
    }
    
    private List<Instance> data ;
    
    public Dataset() {
        data = new ArrayList<>();
    }
    
    public Instance get(int index) {
        return data.get(index);
    }
    
    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty() ;
    }

    @Override
    public boolean contains(Object o) {
        return data.contains(o);
    }

    @Override
    public Iterator<Instance> iterator() {
        return data.iterator() ;
    }

    @Override
    public Object[] toArray() {
        return data.toArray() ;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return data.toArray(a) ;
    }

    @Override
    public boolean add(Instance e) {
        return data.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return data.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return data.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Instance> c) {
        return data.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return data.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return data.retainAll(c);
    }

    @Override
    public void clear() {
        data.clear();
    }
    
}
