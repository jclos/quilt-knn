/*
 * Copyright 2015 Jérémie Clos <j.clos@rgu.ac.uk>.
 *
 * Licensed under the CRAPL License, Version 0.1 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://matt.might.net/articles/crapl/CRAPL-LICENSE.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rgu.ac.uk.knn.data;

import java.util.List;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class Instance {
    private List<String> tok ;
    private String _label ;
    
    public Instance(List<String> tokens, String label) {
        tok = tokens ;
        _label = label ;
    }
    
    public String getLabel() {
        return _label ;
    }
    
    public List<String> getTokens() {
        return tok ;
    }
    
}
