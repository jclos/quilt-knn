/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.ac.uk.knn.data;

import com.google.common.collect.TreeMultiset;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

/**
 * A stack of limited space which only keeps the N top elements. Uses a TreeMultiset as core data structure.
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class TopStack<T> implements Collection<T> {
    private Comparator<T> _c ;
    private int _s ;
    private TreeMultiset<T> _data ;
    
    public TopStack(int size, Comparator<T> c) {
        _c = c ;
        _data = TreeMultiset.create(c);
        _s = size ;
    }
    
    @Override
    public boolean add(T e) {
        boolean state = _data.add(e);
        if (_data.size() > _s) {
            _data.pollLastEntry();
        }
        return state ;
    }

    @Override
    public int size() {
        return _data.size();
    }

    @Override
    public boolean isEmpty() {
        return _data.isEmpty() ;
    }

    @Override
    public boolean contains(Object o) {
        return _data.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return _data.iterator() ;
    }

    @Override
    public Object[] toArray() {
        return _data.toArray() ;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return _data.toArray(a);
    }


    @Override
    public boolean remove(Object o) {
        return _data.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return _data.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean state = true ;
        for (T t : c){
            if (!this.add(t)) {
                state = false ;
            }
        }
        return state ;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean state = true ;
        for (Object t : c){
            if (!this.remove(t)) {
                state = false ;
            }
        }
        return state ;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return _data.removeIf(t -> !c.contains(t)) ;
    }

    @Override
    public void clear() {
        _data.clear();
    }
    
    
    
}
