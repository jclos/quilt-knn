/*
 * Copyright 2015 Jérémie Clos <j.clos@rgu.ac.uk>.
 *
 * Licensed under the CRAPL License, Version 0.1 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://matt.might.net/articles/crapl/CRAPL-LICENSE.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rgu.ac.uk.knn.ml;

import rgu.ac.uk.knn.ml.evaluation.ClassificationStats;
import rgu.ac.uk.knn.data.Instance;
import rgu.ac.uk.knn.data.Aggreg;
import rgu.ac.uk.knn.data.Format;
import rgu.ac.uk.knn.data.Dataset;
import rgu.ac.uk.knn.nlp.BagOfWords;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import rgu.ac.uk.knn.data.TermVector;



/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class KNN {
    private final BagOfWords bow ; 
    private Aggreg.Aggregate aggregation ;
    private int _k ;
    public Memory allInstances ;
    public List<Memory> subSamples ;

    public KNN(int k) {
        _k = k ;
        bow = BagOfWords.createWith(false);
        allInstances = new Memory();
    }
    
    public void setAggregationMechanics(Aggreg.Aggregate ag) {
        aggregation = ag;
    }
    
    public void train(File trainingSet, String separator, Predicate<String> labelFilter, Format.FileFormat training) throws IOException {
        Dataset dataset = training == Format.FileFormat.IAC ? Dataset.readChildrenAndParentsIAC(trainingSet, separator, labelFilter, aggregation, bow) : Dataset.readChildrenAndParentsReddit(trainingSet, separator, labelFilter, aggregation, bow);
        trainParallel(dataset,4);
    }
    
    public static TermVector encode(Instance instance) {
        return new TermVector(instance.getLabel(), 1, instance.getTokens()) ;
    }
    
    public void trainParallel(Dataset dataset, int nBuckets) {
        int bucketSize = (int) Math.ceil(dataset.size() / nBuckets);
        subSamples = new ArrayList<>();
        for (int i = 0 ; i < dataset.size() ; i ++) {
            if (i % bucketSize == 0) {
                subSamples.add(new Memory());
            }
            subSamples.get(subSamples.size() - 1).add(encode(dataset.get(i)));
        }
    }
        
    public void trainLinear(Dataset dataset) {
        for (Instance instance : dataset) { 
            allInstances.add(encode(instance));
        }
    }
    
    public String classifyLinear(Instance instance) throws ExecutionException {
        return allInstances.knn(_k, encode(instance)).getMajorityClass() ;
    }
    
    
    public String classifyParallel(Instance instance) throws ExecutionException, Exception{
        TermVector inst = encode(instance);
        if (subSamples == null) throw new Exception("K-NN has not been trained for parallel classification");
        Memory global = new Memory();
        subSamples.stream().parallel().forEach(mem -> {  
            global.add(mem.knn(_k, inst));
        });
        return global.knn(_k, inst).getMajorityClass() ;
    }

    public ClassificationStats test(File testSet, String separator, String experimentLabel, Predicate<String> labelFilter, Format.FileFormat test) throws IOException, ExecutionException, Exception {
        Dataset dataset = test == Format.FileFormat.IAC ? Dataset.readChildrenAndParentsIAC(testSet, separator, labelFilter, aggregation, bow) : Dataset.readChildrenAndParentsReddit(testSet, separator, labelFilter, aggregation, bow);
        List<String[]> experiment = new ArrayList<>();
        for (Instance instance : dataset) {
            String predicted = this.classifyParallel(instance);
            String[] prediction = {instance.getLabel(), predicted};
            experiment.add(prediction);
        }
        return ClassificationStats.getQuickStats(experiment, experimentLabel);
    }
    
    public ClassificationStats test(Dataset dataset, String experimentLabel) throws ExecutionException {
        List<String[]> experiment = new ArrayList<>();
        for (Instance instance : dataset) {
            String predicted = this.classifyLinear(instance);
            String[] prediction = {instance.getLabel(), predicted};
            experiment.add(prediction);
        }
        return ClassificationStats.getQuickStats(experiment, experimentLabel); 
    }
}
