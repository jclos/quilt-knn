/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.ac.uk.knn.ml;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import rgu.ac.uk.knn.data.TermVector;
import rgu.ac.uk.knn.data.TopStack;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class Memory implements Collection<TermVector> {

    private Set<String> classes;
    private Map<String, Integer> classDistribution;
    private Map<Integer, TermVector> idToInstance;
    private Map<Integer, String> idToClass;
    private Similarity similarity;
    private static int cacheSize = 1000;

    public void add(Memory addendum) {
        addendum.forEach(tv -> {
            this.add(tv);
        });
    }

    public Memory() {
        classes = new HashSet<>();
        classDistribution = new HashMap<>();
        idToInstance = new HashMap<>();
        idToClass = new HashMap<>();
        similarity = new Similarity(cacheSize);
    }
     
    public Memory knn(int k, TermVector instance) {
        Memory mem = new Memory();
        TopStack<TermVector> stack = new TopStack<>(k, new Comparator<TermVector>() {
            @Override
            public int compare(TermVector o1, TermVector o2) {
                return similarity(o1, instance) > similarity(o2, instance) ? -1 : o1 == o2 ? 0 : 1 ;
            }
        });
        for (TermVector tv : this) {
            stack.add(tv);
        }
        mem.addAll(stack);
        return mem ;
    }


    public double similarity(TermVector v1, TermVector v2) {
        try {
            return similarity.similarity(v1, v2);
        } catch (ExecutionException ex) {
            Logger.getLogger(Memory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0d;
    }

    public String getMajorityClass() {
        String maxClass = "";
        int maxInst = Integer.MIN_VALUE;
        for (String vecClass : classDistribution.keySet()) {
            int n = classDistribution.get(vecClass);
            if (n > maxInst) {
                maxClass = vecClass;
                maxInst = n;
            }
        }
        return maxClass;
    }

    @Override
    public int size() {
        return idToInstance.size();
    }

    @Override
    public boolean isEmpty() {
        return idToInstance.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return idToInstance.containsValue(o);
    }

    @Override
    public Iterator<TermVector> iterator() {
        return idToInstance.values().iterator();
    }

    @Override
    public Object[] toArray() {
        return idToInstance.values().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(TermVector e) {
        boolean success = true;
        classes.add(e.getVectorClass());
        classDistribution.put(e.getVectorClass(), classDistribution.getOrDefault(e.getVectorClass(), 0) + 1);
        int currentNumber = idToInstance.size();
        idToInstance.put(currentNumber + 1, e);
        idToClass.put(currentNumber + 1, e.getVectorClass());
        return success;
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return idToInstance.values().containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends TermVector> c) {
        boolean status = true;
        for (Object o : c) {
            TermVector tv = (TermVector) o;
            this.add(tv);
        }
        return status;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        classDistribution = new HashMap<>();
        classes = new HashSet<>();
        idToClass = new HashMap<>();
        idToInstance = new HashMap<>();
    }
}
