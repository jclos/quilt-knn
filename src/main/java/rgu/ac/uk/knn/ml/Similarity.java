/*
 * Copyright 2015 Jérémie Clos <j.clos@rgu.ac.uk>.
 *
 * Licensed under the CRAPL License, Version 0.1 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://matt.might.net/articles/crapl/CRAPL-LICENSE.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rgu.ac.uk.knn.ml;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.util.concurrent.ExecutionException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import rgu.ac.uk.knn.data.TermVector;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class Similarity {
    private LoadingCache<Pair<TermVector, TermVector>, Float> similarityCache ;
    
    public Similarity(int cacheSize) {
        similarityCache = CacheBuilder.newBuilder()
                .maximumSize(cacheSize)
                .build(
                        new CacheLoader<Pair<TermVector, TermVector>, Float>() {
                            public Float load(Pair<TermVector, TermVector> key) {
                                if (key.getLeft().hashCode() > key.getRight().hashCode())
                                {
                                    return TermVector.getSimilarity(key.getLeft(), key.getRight());
                                } else {
                                    return TermVector.getSimilarity(key.getRight(), key.getLeft());
                                }
                            }
                        });
    }
    
    public double similarity(TermVector v1, TermVector v2) throws ExecutionException {
        return (double) similarityCache.get(new ImmutablePair<>(v1, v2));
    }
    
}
