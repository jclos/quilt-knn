package rgu.ac.uk.knn.ml.evaluation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Jeremie Clos
 */
public class ClassificationStats {
    
    public static ClassificationStats getQuickStats(List<String[]> results, String experimentName) {
        Set<String> labels = new HashSet<>();
        List<int[]> res2 = new ArrayList<>();
        for (String[] item : results) {
            labels.add(item[0]);
            labels.add(item[1]);
        }
        Map<String, Integer> classToId = new HashMap<>();
        int i = 0 ;
        for (String _class : labels) {
            classToId.put(_class, i);
            i++ ;
        }
        for (String[] item : results) {
            int[] current = {classToId.get(item[0]), classToId.get(item[1])};
            res2.add(current);
        }
        int j = 0 ;
        String[] stringClasses = new String[classToId.size() + 1];
        stringClasses[0] = experimentName ;
        int[] intClasses = new int[classToId.size()];
        for (Map.Entry<String, Integer> entry : classToId.entrySet()) {
           stringClasses[j + 1] = entry.getKey() ;
           intClasses[j] = entry.getValue() ;
           j ++ ;
        }
        int[][] matrix = ClassificationStats.fromResultsToMatrix(res2, intClasses);
        ClassificationStats res = ClassificationStats.fromMatrix(matrix, stringClasses);
        return res ;
    }
    
    private final Map<Integer, String> classLabels ;
    
    public final int[][] confusionMatrix ;
    public final String label ;
    public final double accuracy ;
    public final double averagePrecision ;
    public final double averageRecall ;
    public final double averageF1Score ;
    
    public final int numberOfErrors ;
    public final int numberOfSuccesses ;
    
    private final double[] precisions ;
    private final double[] recalls ;
    private final double[] f1Scores ;
    
    @Override
    public String toString(){
        boolean labeled = !classLabels.isEmpty() ;
        StringBuilder finalString = new StringBuilder();
        finalString.append("Experiment: ").append(label).append("\n").append("GT\\P\t");
        for (int i = 0; i < confusionMatrix.length; i++) {
            if (labeled){
                finalString.append("P:").append(classLabels.getOrDefault(i, ""+i)).append("\t");
            } else {
                finalString.append("P:").append(i).append("\t");
            }
        }
        finalString.append("\n");
        for (int i = 0 ; i < confusionMatrix.length ; i ++)
        {
            if (labeled){
                finalString.append("GT:").append(classLabels.getOrDefault(i, ""+i)).append("|\t");
            } else {
                finalString.append("GT:").append(i).append("|\t");
            }
            for (int j = 0 ; j < confusionMatrix.length ; j ++){
                finalString.append(confusionMatrix[i][j]).append("\t");
            }
            finalString.append("\n");
        }
        finalString.append("Prec:\t");
        for (int i = 0; i < precisions.length; i++) {
            double round = Math.round(precisions[i] * 1000.0) / 1000.0;
            finalString.append(round).append("\t");
        }
        finalString.append("\n");
        finalString.append("Rec:\t");
        for (int i = 0; i < recalls.length; i++) {
            double round = Math.round(recalls[i] * 1000.0) / 1000.0;
            finalString.append(round).append("\t");
        }
        finalString.append("\n");
        finalString.append("F1S:\t");
        for (int i = 0; i < f1Scores.length; i++) {
            double round = Math.round(f1Scores[i] * 1000.0) / 1000.0;
            finalString.append(round).append("\t");
        }
        finalString.append("\n");
        finalString.append("Classification Accuracy: ").append(accuracy).append("\n");
        finalString.append("Number of correct classifications: ").append(numberOfSuccesses).append("\n");
        finalString.append("Number of misclassifications: ").append(numberOfErrors).append("\n");
        finalString.append("Number of instances: ").append(numberOfErrors + numberOfSuccesses).append("\n");
        finalString.append("Unweighted Average Precision: ").append(averagePrecision).append("\n");
        finalString.append("Unweighted Average Recall: ").append(averageRecall).append("\n");
        finalString.append("Unweighted Average F1-Score: ").append(averageF1Score).append("\n");
        return finalString.toString();
    }
    
    public String toCSV(){
        boolean labeled = !classLabels.isEmpty() ;
        StringBuilder finalString = new StringBuilder();
        finalString.append("Experiment: ").append(label).append("\n").append("Ground Truth\\Predicted,");
        for (int i = 0; i < confusionMatrix.length; i++) {
            if (labeled){
                finalString.append(classLabels.getOrDefault(i, ""+i)).append(",");
            } else {
                finalString.append("Class").append(i).append(",");
            }
        }
        finalString.append("\n");
        for (int i = 0 ; i < confusionMatrix.length ; i ++)
        {
            if (labeled){
                finalString.append(classLabels.getOrDefault(i, ""+i)).append(",");
            } else {
                finalString.append("Class").append(i).append(",");
            }
            for (int j = 0 ; j < confusionMatrix.length ; j ++){
                finalString.append(confusionMatrix[i][j]).append(",");
            }
            finalString.replace(finalString.length() -1 , finalString.length(), "");
            finalString.append("\n");
        }
        finalString.append("Precision,");
        for (int i = 0; i < precisions.length; i++) {
            double round = Math.round(precisions[i] * 1000.0) / 1000.0;
            finalString.append(round).append(",");
        }
        finalString.replace(finalString.length() -1 , finalString.length(), "");
        finalString.append("\n");
        finalString.append("Recall,");
        for (int i = 0; i < recalls.length; i++) {
            double round = Math.round(recalls[i] * 1000.0) / 1000.0;
            finalString.append(round).append(",");
        }
        finalString.replace(finalString.length() -1 , finalString.length(), "");
        finalString.append("\n");
        finalString.append("F1-Score,");
        for (int i = 0; i < f1Scores.length; i++) {
            double round = Math.round(f1Scores[i] * 1000.0) / 1000.0;
            finalString.append(round).append(",");
        }
        finalString.replace(finalString.length() -1 , finalString.length(), "");
        finalString.append("\n");
        return finalString.toString();
    }
    
    /**
     * 
     * @param results list of arrays of results (encoded as integer) of size 2: [ground truth, prediction]
     * @param classes the classes present in the results encoded as integers
     * @return a proper confusion matrix to feed the MLStats
     */
    private static int[][] fromResultsToMatrix(List<int[]> results, int ... classes)
    {
        int n = classes.length ;
        List<Integer> classes2 = new ArrayList<>();
        for (int i : classes) classes2.add(i);
        List<int[]> r = new ArrayList<>();
        
        Map<Integer, Integer> classMapping1 = new HashMap<>();
        int c = 0 ;
        for (int j : classes)
        {
            classMapping1.put(j, c);
            c++ ;
        }
        for (int[] res : results)
        {
            if (res.length == 2 && classes2.contains(res[0]) && classes2.contains(res[1]))
            {
                int[] tmp = new int[2];
                tmp[0] = classMapping1.get(res[0]) ;
                tmp[1] = classMapping1.get(res[1]) ;
                r.add(tmp);
            }
        }
        int[][] matrix = new int[n][n];
        for (int i = 0 ; i < n ; i ++) Arrays.fill(matrix[i], 0);
        for (int[] res : r)
        {
            matrix[res[0]][res[1]] ++ ;
        }
        return matrix ;
    }
    
    
    private ClassificationStats(int[][] data, String ... labels){
        classLabels = new HashMap<>();
        if (labels.length >= 1) {
            this.label = labels[0];
            for (int i = 1 ; i < labels.length ; i ++)
            {
                classLabels.put(i - 1, labels[i]);
            }
        }
        else {
            this.label = "[Generated@" + System.currentTimeMillis() + "]" ;
        }
        this.confusionMatrix = data;
        int numberOfClasses = data.length ;
        precisions = new double[numberOfClasses];
        recalls = new double[numberOfClasses];
        f1Scores = new double[numberOfClasses];
        
        int successes = 0, failures = 0, total = 0 ;
        for (int i = 0 ; i < confusionMatrix.length ; i ++)
        {
            for (int j = 0 ; j < confusionMatrix[i].length ; j++)
            {
                total += confusionMatrix[i][j] ;
                if (i == j) {
                    successes += confusionMatrix[i][j];
                } else {
                    failures += confusionMatrix[i][j];
                }
            }
        }
        numberOfSuccesses = successes ;
        numberOfErrors = failures ;
        accuracy = (double) successes / (double) (successes + failures);
        for (int i = 0 ; i < numberOfClasses ; i ++)
        {
            double precision = 0, recall = 0, f1score = 0;
            int TP = 0, FN = 0, FP = 0 ;
            for (int j = 0 ; j < confusionMatrix[i].length ; j ++)
            {
                if (i == j) TP += confusionMatrix[i][j] ;
                else FN += confusionMatrix[i][j];
            }  
            for (int j = 0 ; j < confusionMatrix[i].length ; j ++)
            {
                if (i != j) FP += confusionMatrix[j][i];
            }
            precision = (double) TP / (double) (FP + TP) ;
            recall = (double) TP / (double) (FN + TP) ;
            f1score = (double) (precision * recall * 2) / (double) (precision + recall);
            precisions[i] = precision ;
            recalls[i] = recall ;
            f1Scores[i] = f1score ;
        }
        double tmp = 0;
        for (double prec : precisions){
            tmp += prec ;
        }
        averagePrecision = (double) tmp / (double) precisions.length ;
        tmp = 0 ;
        for (double rec : recalls){
            tmp += rec ;
        }
        averageRecall = (double) tmp / (double) recalls.length ;
        tmp = 0 ;
        for (double f1 : f1Scores){
            tmp += f1 ;
        }
        averageF1Score = (double) tmp / (double) f1Scores.length ;
    }
    
    
    /**
     * 
     * @param data the confusion matrix. It must be of the following format
     * Actual\Predicted Class1 Class2 Class3
     *           Class1  A      B      C
     *           Class2  D      E      F
     *           Class3  G      H      I
     * Where A + E + I are correct classifications
     * @param labels the labels in the experiment. First label is for the experiment,
     * next labels are for the class, by order of appearance (Class1, Class2, Class3)
     * @return 
     */
    private static ClassificationStats fromMatrix(int[][] data, String ... labels)
    {
        return new ClassificationStats(data, labels);
    }
}
