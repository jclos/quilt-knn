/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.ac.uk.knn.ml.evaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import rgu.ac.uk.knn.data.Dataset;
import rgu.ac.uk.knn.data.Instance;
import rgu.ac.uk.knn.ml.KNN;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class Test {
    public static void main(String[] args) throws ExecutionException, Exception {
        KNN knn = new KNN(1);
        Dataset training = new Dataset();
        
        List<String> i1terms = new ArrayList<>();
        i1terms.add("I");
        i1terms.add("don't");
        i1terms.add("like");
        i1terms.add("vanilla");
        i1terms.add("candy");
        Instance i1 = new Instance(i1terms, "negative");
        training.add(i1);
        List<String> i2terms = new ArrayList<>();
        i2terms.add("you");
        i2terms.add("don't");
        i2terms.add("like");
        i2terms.add("vanilla");
        i2terms.add("candy");
        Instance i2 = new Instance(i2terms, "negative");
        training.add(i2);

        List<String> i3terms = new ArrayList<>();
        i3terms.add("I");
        i3terms.add("enjoy");
        i3terms.add("dark");
        i3terms.add("chocolate");
        i3terms.add("candy");
        Instance i3 = new Instance(i3terms, "positive");
        training.add(i3);
        
        List<String> i4terms = new ArrayList<>();
        i4terms.add("I");
        i4terms.add("really");
        i4terms.add("like");
        i4terms.add("walking");
        i4terms.add("outside");
        Instance i4 = new Instance(i4terms, "positive");
        training.add(i4);
        
        List<String> i5terms = new ArrayList<>();
        i5terms.add("I");
        i5terms.add("don't");
        i5terms.add("enjoy");
        i5terms.add("violent");
        i5terms.add("movies");
        Instance i5 = new Instance(i5terms, "negative");
        training.add(i5);
        
        List<String> i6terms = new ArrayList<>();
        i6terms.add("Are");
        i6terms.add("you");
        i6terms.add("that");
        i6terms.add("stupid");
        i6terms.add("?");
        Instance i6 = new Instance(i6terms, "negative");
        training.add(i6);
          
        List<String> i7terms = new ArrayList<>();
        i7terms.add("I");
        i7terms.add("like");
        i7terms.add("your");
        i7terms.add("new");
        i7terms.add("haircut");
        Instance i7 = new Instance(i7terms, "positive");
        training.add(i7);
        
        Dataset testing = new Dataset();
        
        List<String> i8terms = new ArrayList<>();
        i8terms.add("do");
        i8terms.add("you");
        i8terms.add("like");
        i8terms.add("them");
        i8terms.add("apples");
        Instance i8 = new Instance(i8terms, "negative");
        testing.add(i8);
        
        List<String> i9terms = new ArrayList<>();
        i9terms.add("I");
        i9terms.add("like");
        i9terms.add("your");
        i9terms.add("chocolate");
        i9terms.add("candy");
        Instance i9 = new Instance(i9terms, "positive");
        testing.add(i9);
        
        knn.trainParallel(training,3);  
        System.out.println("Successful classification: " + (knn.classifyParallel(i1) == i1.getLabel()));
        System.out.println("Successful classification: " + (knn.classifyParallel(i2) == i2.getLabel()));
        System.out.println("Successful classification: " + (knn.classifyParallel(i3) == i3.getLabel()));
        System.out.println("Successful classification: " + (knn.classifyParallel(i4) == i4.getLabel()));
        System.out.println("Successful classification: " + (knn.classifyParallel(i5) == i5.getLabel()));
        System.out.println("Successful classification: " + (knn.classifyParallel(i6) == i6.getLabel()));
        System.out.println("Successful classification: " + (knn.classifyParallel(i7) == i7.getLabel()));
    }
}
