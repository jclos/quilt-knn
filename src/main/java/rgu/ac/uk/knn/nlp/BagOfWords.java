package rgu.ac.uk.knn.nlp;

import edu.stanford.nlp.util.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Jeremie Clos
 */
public class BagOfWords {
     
    public final boolean _useNLP ;
    private final boolean _kp ;
    private final SentenceSplitter _ss ;
    private final TextSplitter _ts ;
    
    private BagOfWords(boolean useNLP, boolean keepPunc) {
        _useNLP = useNLP ;
        _kp = keepPunc ;
        _ss = new SentenceSplitter();
        _ts = new TextSplitter();
    }
    
    public static Map<String, Integer> merge(Map<String, Integer> ... bows) {
        Map<String, Integer> result = new HashMap<>();
        for (Map<String, Integer> bow : bows) {
            for (Map.Entry<String, Integer> w : bow.entrySet()) {
                String token = w.getKey() ;
                int freq = w.getValue() ;
                int prevFreq = result.getOrDefault(token, 0);
                result.put(token, prevFreq + freq);
            }
        }
        return result ;
    }
    
    public static Map<String, Integer> intersect(Map<String, Integer> bow1, Map<String, Integer> bow2) {
        Map<String, Integer> result = new HashMap<>();
        for (Map.Entry<String, Integer> w : bow1.entrySet()) {
            String token = w.getKey();
            int freq1 = w.getValue();
            if (bow2.containsKey(token)) {
                int freq2 = bow2.get(token);
                result.put(token, freq1 + freq2);
            }
        }
        return result ;
    }
    
    public static BagOfWords createWith(boolean useNLP, boolean keepPunctuation) {
        return new BagOfWords(useNLP, keepPunctuation);
    }
    
    public static BagOfWords createWith(boolean keepPunctuation) {
        return new BagOfWords(true, keepPunctuation);
    }
    
    public Set<String> getBOW(String text) {
        Set<String> result = new HashSet<>();
        if (_useNLP) {
            result = parseNLP(text);
        } else {
            result = parseSimple(text);
        }
        return result ;
    }
    
    public List<String> tokenize(String text) {
        List<String> result = new ArrayList<>();
        if (_useNLP) {
            result = tokenizeNLP(text);
        } else {
            result = tokenizeSimple(text);
        }
        return result;
    }
    
    private List<String> tokenizeSimple(String text) {
        List<String> tokens = new ArrayList<>();
        for (String sentence : _ts.fastSplit(text)) {
            for (String token : _ss.simpleSplit(sentence)) if (!token.trim().isEmpty()) {
                tokens.add(token.toLowerCase());
            }
        }
        if (_kp) return tokens ;
        else return tokens.stream().filter(exp -> !isPunctuation(exp)).collect(Collectors.toList());
    }
    
    private List<String> tokenizeNLP(String text) {
        List<String> tokens = new ArrayList<>();
        for (String sentence : _ts.nlpSplit(text)) {
            for (String token : _ss.splitDeeper(sentence)) if (!token.trim().isEmpty()) {
                tokens.add(token.toLowerCase());
            }
        }
        if (_kp) return tokens ;
        else return tokens.stream().filter(exp -> !isPunctuation(exp)).collect(Collectors.toList());
    }
    
    public Map<String, Integer> getBOWWithFrequencies(String text) {
        Map<String, Integer> result = new HashMap<>();
        if (_useNLP) {
            result = parseNLPWithFrequencies(text);
        } else {
            result = parseSimpleWithFrequencies(text);
        }
        return result;
    }
    
    private Map<String, Integer> parseSimpleWithFrequencies(String text) {
        Map<String, Integer> result = new HashMap<>(); 
        for (String sentence : _ts.fastSplit(text)) {
            for (String token : _ss.simpleSplit(sentence)) if (!token.trim().isEmpty()) {
                int freq = result.getOrDefault(token, 0) + 1;
                result.put(token, freq);
            }
        }
        if (_kp) return result ;
        else return result.entrySet().stream().filter((entry) -> !isPunctuation(entry.getKey())).collect(Collectors.toMap(Map.Entry::getKey , Map.Entry::getValue, (a,b) -> a, HashMap::new));
    }
    
    private Map<String, Integer> parseNLPWithFrequencies(String text) {
        Map<String, Integer> result = new HashMap<>(); 
        for (String sentence : _ts.nlpSplit(text)) {
            for (String token : _ss.splitDeeper(sentence)) if (!token.trim().isEmpty()) {
                int freq = result.getOrDefault(token, 0) + 1;
                result.put(token, freq);
            }
        }
        if (_kp) return result ;
        else return result.entrySet().stream().filter((entry) -> !isPunctuation(entry.getKey())).collect(Collectors.toMap(Map.Entry::getKey , Map.Entry::getValue, (a,b) -> a, HashMap::new));
    }
    
    private Set<String> parseSimple(String text) {
        Set<String> bow = new HashSet<>();
        for (String sentence : _ts.fastSplit(text)) {
            for (String token : _ss.simpleSplit(sentence)) if (!token.trim().isEmpty()) {
                bow.add(token.toLowerCase());
            }
        }
        if (_kp) return bow ;
        else return bow.stream().filter(exp -> !isPunctuation(exp)).collect(Collectors.toSet());
    }
    
    private Set<String> parseNLP(String text) {
        Set<String> bow = new HashSet<>();
        for (String sentence : _ts.nlpSplit(text)) {
            for (String token : _ss.splitDeeper(sentence)) if (!token.trim().isEmpty()) {
                bow.add(token.toLowerCase());
            }
        }
        if (_kp) return bow ;
        else return bow.stream().filter(exp -> !isPunctuation(exp)).collect(Collectors.toSet());
    }
    
    private static boolean isPunctuation(String exp) {
        return StringUtils.isPunct(exp) ;
    }
}
