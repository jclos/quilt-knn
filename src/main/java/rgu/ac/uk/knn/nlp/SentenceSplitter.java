package rgu.ac.uk.knn.nlp;


import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

/**
 *
 * @author Jeremie Clos
 */
public class SentenceSplitter {
    private final Pattern _r ;
    private final Random _seed ;
    private final Map<String, List<String>> _cache ;
    private final List<String> _cacheEntries ;
    private final int _sizeCache ;
    private final String pattern = "\\b";

    public SentenceSplitter() {
        _seed = new Random();
        _sizeCache = 0 ;
        _cache = new HashMap<>();
        _cacheEntries = new ArrayList<>();
        _r = Pattern.compile(pattern);
    }
    
    public SentenceSplitter(int sizeCache) {
        _seed = new Random();
        _sizeCache = sizeCache ;
        _cache = new HashMap<>();
        _cacheEntries = new ArrayList<>();
        _r = Pattern.compile(pattern);
    }
    
    /**
     * Applies Stanford tokenization
     * @param sentence
     * @return list of terms
     */
    public List<String> split(String sentence) {
        List<String> result = new ArrayList<>();
        PTBTokenizer ptbt = new PTBTokenizer(new StringReader(sentence), new CoreLabelTokenFactory(), "");
        for (CoreLabel label; ptbt.hasNext();) {
            label = (CoreLabel) ptbt.next();
            result.add(label.value());
        }
        return result;
    }
    
    /**
     * Does a simple tokenization based on a regular expression
     * @param sentence
     * @return list of terms
     */
    public List<String> simpleSplit(String sentence) {
        List<String> result = new ArrayList<>();
        result.addAll(Arrays.asList(_r.split(sentence)));
        return result ;
    }
    
    /**
     * Applies Stanford tokenization, caches the results
     * @param sentence
     * @return list of terms
     */
    public List<String> cachedSplit(String sentence) {
        if (_cache.containsKey(sentence)) // check if we didn't already tokenize this sentence somewhere
        {
            return _cache.get(sentence);
        }
        List<String> result = new ArrayList<>();
        PTBTokenizer ptbt = new PTBTokenizer(new StringReader(sentence), new CoreLabelTokenFactory(), "");
        for (CoreLabel label; ptbt.hasNext();) {
            label = (CoreLabel) ptbt.next();
            result.add(label.value());
        }
        if (_cache.size() <= _sizeCache)
        {
            _cache.put(sentence, result);
            _cacheEntries.add(sentence);
        }
        else
        {
            //Removes a random element from the cache
            String toRemove = _cacheEntries.get(_seed.nextInt(_cacheEntries.size()));
            _cache.remove(toRemove);
            _cacheEntries.remove(toRemove);
            _cache.put(sentence, result);
            _cacheEntries.add(sentence);
        }
        return result;
    }
    
    /**
     * Splits into tokens, then resplits again to take care of things like something/somethingElse
     * @param sentence
     * @return list of tokens
     */
    public List<String> splitDeeper(String sentence)
    {
        List<String> result = new ArrayList<>();
        for (String termCandidate : split(sentence))
        {
            result.addAll(Arrays.asList(termCandidate.split("/")));
        }
        return result;
    }
    
    /**
     * Applies Stanford CoreNLP tokenization
     * @param sentence
     * @return list of tokens
     */
    public List<Word> tokenize(String sentence)
    {
        List<Word> result = new ArrayList<>();
        for (String termCandidate : splitDeeper(sentence))
        {
            result.add(new Word(termCandidate));
        }
        return result ;
    }
}
