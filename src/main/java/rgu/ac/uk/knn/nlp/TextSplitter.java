package rgu.ac.uk.knn.nlp;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author Jeremie Clos
 */
public class TextSplitter {

    private final StanfordCoreNLP corenlp;

    public TextSplitter() {
        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit");
        corenlp = new StanfordCoreNLP(props);
    }

    /**
     * Splits based on Stanford CoreNLP splitter
     * @param text
     * @return list of sentences
     */
    public List<String> nlpSplit(String text) {
        List<String> result = new ArrayList<>();
        Annotation document = new Annotation(text);
        corenlp.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            result.add(sentence.toString());
        }
        return result;
    }
    
    /**
     * Splits on full stops or exclamation marks or interrogation marks. Should
 theoretically be faster than normal nlpSplit.
     * @param text
     * @return list of sentences
     */
    public List<String> fastSplit(String text)
    {
        return Arrays.asList(text
                .replaceAll("(\\.)+","\\.") // collapses full stops
                .replaceAll("(\\?)+", "\\?") // collapses interrogation marks
                .replaceAll("(!)+", "!") // collapses exclamation marks
                .replaceAll("!(\\?)+", "!") // collapses !?, !??, etc.
                .replaceAll("\\?(!)+", "\\?") // collapses ?!, ?!!, etc.
                .split("\\.|!|\\?")); // splits
    }

}
